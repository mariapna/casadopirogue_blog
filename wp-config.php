<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'projetos_casadopirogue_blog' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'J?[C8%&0|F(3;w)OT@io*93?%tz{/O]Ss5do[]WZU3*nN8M#h/F,4CgAA(ui9xHI' );
define( 'SECURE_AUTH_KEY',  'r`x[#(}k2!5g[Av#hs:gq<stHD_A.qIKnf>OJN7$Bt(fHQ55?er?~ck]0zBsK7>R' );
define( 'LOGGED_IN_KEY',    'smy8Gahx;/t(_zKK,GzTyXc8DZ/e?W-)Pmos&{-ti:y|=qsOF2~^Y(zN@uw6[f_Z' );
define( 'NONCE_KEY',        'u4]Wu/z{(gJAh:A/d#`=.o!pn`!jJ5_(M~2LP-&CWTyhcY?9>gyQ0ar6,/vY)]fp' );
define( 'AUTH_SALT',        'x+BWhonHdS J!&w}jB/&CG_Mz05G-],0YZ,SKdJh2m7TpvwTv8yXtogo=W!,d77R' );
define( 'SECURE_AUTH_SALT', 'Fg%x.Vc,KSLW{vu}-1@Xj[<0@[9IG`qyIp+Q.SSzBbby9.<;f/Cr%3}Ch&+7=hY/' );
define( 'LOGGED_IN_SALT',   ')JITN%m*%B:?}vW|D%7)[L}6a$A}--XBM`8kH6l^gP69<jOCx?TH.n@W{w?_[YIp' );
define( 'NONCE_SALT',       '}.y?Yg{trJ5!trKg6]DI-Ks<Z=~UpDWlK2>MFIoM.n&d`waI&{Z21PH^pkzB>MJy' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'cp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
