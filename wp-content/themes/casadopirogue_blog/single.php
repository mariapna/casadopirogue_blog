<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package CasaDoPirogue
 */

$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$fotoPost = $fotoPost[0];

$categoriaPost = get_the_category();
foreach ($categoriaPost as $categoriaPost) {
	$categoriaPostNome = $categoriaPost->name;
	$categoriaPostId = $categoriaPost->cat_ID;
}
$idPostAtual = $post->ID;

get_header();
?>

		<!-- PG DE POST -->
		<div class="pg pg-post">
			<div class="containerFullPgPost">
				<div class="row">
					<div class="col-sm-8">
						<div class="conteudo-post">
							<div class="destaque">
								<img src="<?php echo $fotoPost; ?>" alt="<?php echo the_title(); ?>">
								<span class="categoria"><?php echo $categoriaPostNome ?></span>
							</div>
							<span class="data"><?php echo get_the_date('j M Y') ?></span>
							<h1><?php echo get_the_title(); ?></h1>
							<div class="texto">
								<p><?php echo get_the_content(); ?></p>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<!-- COLUNA SIDEBAR -->
						<?php get_sidebar(); ?>
					</div>
				</div>

				<div class="relacionados">
					<h3>Semelhantes</h3>
					<ul>
						<?php 
						$contador = 0;
						$relacionados = new WP_Query(array('post_type' => 'post', 'orderby' => 'id', 'posts_per_page' => -1));

						if($relacionados->have_posts()):
							while($relacionados->have_posts()): $relacionados->the_post();

							$fotoPostsRelacionados = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');
							$fotoPostsRelacionados = $fotoPostsRelacionados[0];
							$idPostRelacionado = $post->ID;
							
							$categoriaRelacionados = get_the_category();
							foreach ($categoriaRelacionados as $categoriaRelacionados){
								$categoriaRelacionadosNome=$categoriaRelacionados->name;
								$categoriaRelacionadosId=$categoriaRelacionados->cat_ID;
							}

							if($categoriaRelacionadosId == $categoriaPostId && $idPostAtual != $idPostRelacionado && $contador < 3): ?>
						<li>
							<a href="<?php echo get_permalink(); ?>">
								<figure>
									<img src="<?php echo $fotoPostsRelacionados; ?>" alt="<?php echo the_title(); ?>">
								</figure>
								<div class="titulo">
									<span class="categoria"><?php echo $nomeCategoriaRelacionado; ?></span>
									<h4><?php echo get_the_title(); ?></h4>
								</div>
							</a>
						</li>
						<?php $contador++; endif; endwhile; endif; wp_reset_query(); ?>
					</ul>
				</div>

				<div id="disqus"></div>
			</div>
		</div>

<?php

get_footer();
