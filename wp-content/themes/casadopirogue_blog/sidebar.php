<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package CasaDoPirogue
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}

?>

<!-- COLUNA SIDEBAR -->
<div class="sidebar">
	<!-- NEWSLETTER -->
	<div class="newsletter">
		<h3>Assine nossa Newsletter!</h3>
		<p>E fique por dentro de todas as novidades! Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
		<div class="formulario">
			<!-- <input type="text" name="name" placeholder="Nome">
			<input type="text" name="email" placeholder="E-mail">
			<select name="como-nos-conheceu">
				<option value="Como nos conheceu?">Como nos conheceu?</option>
				<option value="Já sou cliente">Já sou cliente</option>
				<option value="Indicação">Indicação</option>
				<option value="Google">Google</option>
				<option value="Facebook">Facebook</option>
				<option value="Instagram">Instagram</option>
				<option value="Outro">Outro</option>
			</select>
			<input type="submit" name="submit" value="Enviar"> -->

			<?php echo do_shortcode('[contact-form-7 id="5" title="Newsletter"]'); ?>
		</div>
	</div>

	<div class="faca-seu-pedido">
		<a href="#">
			<p><i class="fas fa-hand-point-down"></i></p>
			<h2>Faça seu pedido online, agora mesmo!</h2>
			<!-- <p><i class="fas fa-hand-point-up"></i></p> -->
		</a>
	</div>

	<!-- POSTS MAIS VISTOS -->
	<div class="mais-posts mais-vistos">
		<h3>Mais vistos</h3>
		<ul class="lista">
			<?php
			$posts_mais_vistos = new WP_Query( 
				array(
			        'posts_per_page'      => 3,                 // Máximo de 3 artigos
			        'no_found_rows'       => true,              // Não conta linhas
			        'post_status'         => 'publish',         // Somente posts publicados
			        'ignore_sticky_posts' => true,              // Ignora posts fixos
			        'orderby'             => 'meta_value_num',  // Ordena pelo valor da post meta
			        'meta_key'            => 'tp_post_counter', // A nossa post meta
			        'order'               => 'DESC'             // Ordem decrescente
			    )
			);

			if($posts_mais_vistos->have_posts()):
				while($posts_mais_vistos->have_posts()): $posts_mais_vistos->the_post();
			?>
			<li class="lista-li">
				<a href="#">
					<p class="titulo"><?php echo get_the_title(); ?></p>
					<?php $categoriaPostsMaisVistos = get_the_category();
					foreach($categoriaPostsMaisVistos as $categoria): ?>
					<span class="categoria"><?php echo $categoria->name ?></span>
					<?php endforeach; ?>
				</a>
			</li>
			<?php endwhile; endif; wp_reset_postdata(); ?>
		</ul>
	</div>

	<!-- CATEGORIAS -->
	<div class="categorias">
		<h3>Categorias</h3>
		<ul class="lista-categorias">
			<?php
			$categorias = get_categories();
			foreach($categorias as $categoria):
			?>
			<li class="categorias-li">
				<a href="<?php echo get_category_link($categoria->cat_ID); ?>">
					<p><?php echo $categoria->name; ?></p>
				</a>
			</li>
			<?php endforeach; ?>
		</ul>
	</div>
</div>
