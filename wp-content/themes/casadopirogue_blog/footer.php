<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package CasaDoPirogue
 */

global $configuracao;
 $endereco = $configuracao['opt-endereco-rua'];
 $telefone = $configuracao['opt-telefone'];
 $cidade = $configuracao['opt-endereco-cidade'];
 $cep = $configuracao['opt-endereco-cep'];
 $cnpj = $configuracao['opt-cnpj'];
 $horariosSS = $configuracao['opt-horariosSS'];
 $horariosS = $configuracao['opt-horariosS'];
 $facebook = $configuracao['opt-facebook'];
 $instagram = $configuracao['opt-instagram'];
 $google = $configuracao['opt-google'];

?>

	<footer class="rodape">
		<div class="container">
			<ul>
				<li>
					<span>Casa do Pirogue</span>
					<p><?php echo $endereco ?></p>
					<p><?php echo $cidade  ?></p>
					<p>CEP:<?php echo $cep ?></p>
					<p>CNPJ:<?php echo $cnpj ?></p>
				</li>
				<li>
					<span>Horários de atendimento</span>
					<p>Seg-Sex:</p>
					<p>Das <?php echo $horariosSS ?></p>
					<p>Sáb:</p>
					<p>Das <?php echo $horariosS ?></p>
				</li>
					
				<li>
					<span>Empresa</span>
					<a href="<?php echo home_url('/sobre-nos/'); ?>">Sobre nós</a>
					<a href="<?php echo home_url('/politica-de-privacidade/'); ?>">Política de Privacidade</a>
					<a href="<?php echo home_url('/fretes-e-entregas/'); ?>">Fretes e Entregas</a>
					<a href="<?php if (is_front_page()) { echo "#area-contato";} else { echo home_url('/#area-contato');} ?>" id="contato-footer">Fale Conosco</a>
				</li>

				<li>
					<span>Minha Conta</span>
					<a href="https://delivery.casadopirogue.com.br/dashboard/">Acesse sua conta</a>
					<a href="https://delivery.casadopirogue.com.br/my-orders/">Meus Pedidos</a>
				
				</li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-6">			
				<section class="redes-sociais">
					
					<div class="icon">
						<a class="hvr-rotate"  href="<?php echo  $facebook ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						<a  class="hvr-rotate" href="<?php echo  $instagram ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
						<a class="hvr-rotate"  href="<?php echo  $google ?>" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
					</div>
				</section>
			</div>
			<div class="col-md-6">	

				<div class="bandeiras">
					<img class="hvr-grow" src="<?php bloginfo('template_url'); ?>/img/b3.png" alt="">
					<img class="hvr-grow" src="<?php bloginfo('template_url'); ?>/img/b4.png" alt="">
					<img class="hvr-grow" src="<?php bloginfo('template_url'); ?>/img/b5.png" alt="">
					<img class="hvr-grow" src="<?php bloginfo('template_url'); ?>/img/b6.png" alt="">
					<img class="hvr-grow" src="<?php bloginfo('template_url'); ?>/img/b7.png" alt="">
				</div>

			</div>
		</div>	
		<div class="copyright">
				<i class="fa fa-copyright" aria-hidden="true"></i><p>Casa do Pirogue - Todos os direitos reservados</p>
		</div>
		
	</footer>
	<div class="row developers">
			<div class="col-md-6 area text-center">
				<span>Desenvolvido por</span>
				<a href="http://hudsoncarolino.com.br/" title="Desenvolvido por Rehnan Carolino e Hudson Carolino">
					devcarolino
				</a>
			</div>
			<div class="col-md-6 area text-center">				
				<span>Designer</span>
				<a href="https://www.facebook.com/dldesignerc/?fref=ts" target="_blank">
					dl designer
				</a>
			</div>
		</div>

<?php wp_footer(); ?>

</body>
</html>