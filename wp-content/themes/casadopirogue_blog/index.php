<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package CasaDoPirogue
 */

get_header();

	// RECUPERANDO CATEGORIAS PAI
	// $categoriasPosts = array(
	// 	'taxonomy'     => 'category',
	// 	'child_of'     => 0,
	// 	'parent'       => 0,
	// 	'orderby'      => 'name',
	// 	'pad_counts'   => 0,
	// 	'hierarchical' => 1,
	// 	'title_li'     => '',
	// 	'hide_empty'   => 0
	// );
	// $listaCategoriasPosts = get_categories($categoriasPosts);

$listaCategoriasPosts = get_categories();

?>

<!-- PG INICIAL -->
<div class="pg pg-inicial">
	<!-- CARROSSEL DE DESTAQUE -->
	<section class="secao-destaque sessao">
		<div class="containerFull">
			<div class="carrosselDestaque">

				<!-- ITEM -->
				<div class="item" style="">
					<?php
					$contador = 0;

					foreach($listaCategoriasPosts as $listaCategoriasPosts): 
						$listaCategoriasPosts = $listaCategoriasPosts;

						if($contador == 0):
					?>

					<a href="<?php echo get_category_link($listaCategoriasPosts->cat_ID) ?>" class="large">
						<figure style="background-image: url(<?php echo z_taxonomy_image_url($listaCategoriasPosts->cat_ID); ?>)">
							<img class="img-responsive" src="<?php echo z_taxonomy_image_url($listaCategoriasPosts->cat_ID) ?>" alt="<?php ?>">
						</figure>
						<div class="conteudo">
							<h4><?php echo $listaCategoriasPosts->name ?></h4>
						</div>
					</a>

					<?php elseif ($contador == 1): ?>
					<a href="<?php echo get_category_link($listaCategoriasPosts->cat_ID) ?>" class="small small-top">
						<figure style="background-image: url(<?php echo z_taxonomy_image_url($listaCategoriasPosts->cat_ID) ?>)">
							<img class="img-responsive" src="<?php echo z_taxonomy_image_url($listaCategoriasPosts->cat_ID) ?>" alt="<?php ?>">
						</figure>
						<div class="conteudo">
							<h4><?php echo $listaCategoriasPosts->name ?></h4>
						</div>
					</a>
					<?php elseif ($contador == 2): ?>
					<a href="<?php echo get_category_link($listaCategoriasPosts->cat_ID) ?>" class="small small-bottom">
						<figure style="background-image: url(<?php echo z_taxonomy_image_url($listaCategoriasPosts->cat_ID) ?>)">
							<img class="img-responsive" src="<?php echo z_taxonomy_image_url($listaCategoriasPosts->cat_ID) ?>" alt="<?php ?>">
						</figure>
						<div class="conteudo">
							<h4><?php echo $listaCategoriasPosts->name ?></h4>
						</div>
					</a>
					<?php endif; $contador++; endforeach; ?>
				</div>
				<!-- ITEM -->
				<div class="item" style="">
					<?php
					$contador2 = 0;

					$listaCategoriasPosts = get_categories();
					foreach($listaCategoriasPosts as $listaCategoriasPosts): 
						$listaCategoriasPosts = $listaCategoriasPosts;

						if($contador2 == 3):
					?>

					<a href="<?php echo get_category_link($listaCategoriasPosts->cat_ID) ?>" class="large">
						<figure style="background-image: url(<?php echo z_taxonomy_image_url($listaCategoriasPosts->cat_ID) ?>)">
							<img class="img-responsive" src="<?php echo z_taxonomy_image_url($listaCategoriasPosts->cat_ID) ?>" alt="<?php ?>">
						</figure>
						<div class="conteudo">
							<h4><?php echo $listaCategoriasPosts->name ?></h4>
						</div>
					</a>

					<?php elseif ($contador2 == 4): ?>
					<a href="<?php echo get_category_link($listaCategoriasPosts->cat_ID) ?>" class="small small-top">
						<figure style="background-image: url(<?php echo z_taxonomy_image_url($listaCategoriasPosts->cat_ID) ?>)">
							<img class="img-responsive" src="<?php echo z_taxonomy_image_url($listaCategoriasPosts->cat_ID) ?>" alt="<?php ?>">
						</figure>
						<div class="conteudo">
							<h4><?php echo $listaCategoriasPosts->name ?></h4>
						</div>
					</a>
					<?php elseif ($contador2 == 5): ?>
					<a href="<?php echo get_category_link($listaCategoriasPosts->cat_ID) ?>" class="small small-bottom">
						<figure style="background-image: url(<?php echo z_taxonomy_image_url($listaCategoriasPosts->cat_ID) ?>)">
							<img class="img-responsive" src="<?php echo z_taxonomy_image_url($listaCategoriasPosts->cat_ID) ?>" alt="<?php ?>">
						</figure>
						<div class="conteudo">
							<h4><?php echo $listaCategoriasPosts->name ?></h4>
						</div>
					</a>
					<?php endif; $contador2++; endforeach; ?>
				</div>
			</div>
		</div>
	</section>

<!-- 	<section class="secao-categorias">
		<h6 class="hidden">SEÇÃO DE CATEGORIAS</h6>
		<div class="containerFull">
			<ul>
				<?php //$contador = 0;
				//foreach($listaCategoriasPosts as $listaCategoriasPosts): 
					//$listaCategoriasPosts = $listaCategoriasPosts;
					//if($contador < 3): 
				?>
				<li>
					<a href="<?php// echo get_category_link($listaCategoriasPosts->cat_ID); ?>">
						<img src="<?php //echo z_taxonomy_image_url($listaCategoriasPosts->cat_ID); ?>" alt="">
						<span class="categoria"><?php //echo $listaCategoriasPosts->name; ?></span>
						<span class="ver-mais">Ver mais</span>
					</a>
				</li>
				<?php //$contador++; endif; endforeach; ?>
			</ul>
		</div>
	</section> -->

	<!-- SEÇÃO DE POSTS -->
	<section class="secao-posts">
		<h6 class="hidden">SEÇÃO DE POSTS</h6>
		<div class="containerFullSecaoPosts">
			<div class="row">
				<div class="col-sm-8">

					<!-- COLUNA DE POSTS -->
					<div class="posts">
						<div class="pesquisa">
							<form action="<?php echo home_url('/'); ?>" role="search" method="get">
								<input type="text" name="s" placeholder="Pesquisar">
							</form>
						</div>
						<ul>
							<?php
							if(have_posts()): while(have_posts()): the_post();
								$fotoPost = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
								$fotoPost = $fotoPost[0];
							?>
							<li class="post">
								<a href="<?php echo get_permalink(); ?>">
									<figure class="foto-destaque">
										<img src="<?php echo $fotoPost; ?>" alt="<?php echo the_title(); ?>">
									</figure>
									<div class="conteudo">
										<h2><?php echo get_the_title(); ?></h2>
										<p><?php echo customExcerpt(130); ?></p>
										<span>Ver post</span>
									</div>
								</a>
							</li>
							<?php endwhile; endif; wp_reset_query(); ?>
						</ul>
					</div>

				</div>
				<div class="col-sm-4">
					<!-- COLUNA SIDEBAR -->
					<?php get_sidebar(); ?>
				</div>
			</div>

			<?php if(function_exists('pagination')){
				pagination($additional_loop->$max_num_pages);
			} ?>

		</div>
	</section>

	<!-- SEÇÃO PRODUTOS -->
	<section class="secao-produtos">
		<h6 class="hidden">SEÇÂO DE PRODUTOS</h6>
		<div class="containerFull">
			<h2>Confira nossos produtos!</h2>

			<div class="row">
				<div class="col-sm-6">
					<div class="produto pirogue">
						<a href="#">
							<figure style="background-image: url(<?php echo get_template_directory_uri() ?>/img/pirogue.jpg)">
								<img src="<?php echo get_template_directory_uri() ?>/img/pirogue.jpg" alt="">
							</figure>
							<span class="nome-produto">Pirogue</span>
							<span class="abrir-opcoes">Ver opções</span>
						</a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="produto pastel">
						<a href="#">
							<figure style="background-image: url(<?php echo get_template_directory_uri() ?>/img/pastel.jpg)">
								<img src="<?php echo get_template_directory_uri() ?>/img/pastel.jpg" alt="">
							</figure>
							<span class="nome-produto">Pastel</span>
							<span class="abrir-opcoes">Ver opções</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- INSTAGRAM -->
	<section class="secao-instagram">
		<h6 class="hidden">SEÇÃO INSTAGRAM</h6>
		<div class="containerFull">
			<h2><a href="#">Confira nosso instagram! @casadopirogue</a></h2>
			<div id="instafeed"></div>
		</div>
	</section>
</div>

<?php

get_footer();
