<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package CasaDoPirogue
 */

get_header();
?>

<div class="pg pg-inicial">
	<!-- SEÇÃO DE POSTS -->
	<section class="secao-posts">
		<h6 class="hidden">SEÇÃO DE POSTS</h6>
		<div class="containerFullSecaoPosts">
			<div class="row">
				<div class="col-sm-8">

					<!-- COLUNA DE POSTS -->
					<div class="posts">
						<div class="pesquisa">
							<form action="" role="search" method="get">
								<input type="text" name="search" placeholder="Pesquisar">
							</form>
						</div>
						<ul>
							<?php
							if(have_posts()): while(have_posts()): the_post();
								$fotoPost = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
								$fotoPost = $fotoPost[0];
							?>
							<li class="post">
								<a href="<?php echo get_permalink(); ?>">
									<figure class="foto-destaque">
										<img src="<?php echo $fotoPost; ?>" alt="<?php echo the_title(); ?>">
									</figure>
									<div class="conteudo">
										<h2><?php echo get_the_title(); ?></h2>
										<p><?php echo customExcerpt(130); ?></p>
										<span>Ver post</span>
									</div>
								</a>
							</li>
							<?php endwhile; endif; wp_reset_query(); ?>
						</ul>
					</div>

				</div>
				<div class="col-sm-4">
					<!-- COLUNA SIDEBAR -->
					<?php get_sidebar(); ?>
				</div>
			</div>

			<?php if(function_exists('pagination')){
				pagination($additional_loop->$max_num_pages);
			} ?>

		</div>
	</section>

	<!-- INSTAGRAM -->
	<!-- <section class="secao-instagram">
		<h6 class="hidden">SEÇÃO INSTAGRAM</h6>
		<div class="containerFull">
			<h2><a href="#">Confira nosso instagram! @casadopirogue</a></h2>
			<div id="instafeed"></div>
		</div>
	</section> -->
</div>

<?php
get_footer();
